// Name: Alejandro Monroy Reyes
// USC loginid: monroyre
// CS 455 PA3
// Fall 2016

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JComponent;
import java.awt.Rectangle;
import java.util.ListIterator;

/**
   MazeComponent class
   
   A component that displays the maze and path through it if one has been found.
*/
public class MazeComponent extends JComponent
{
   private Maze maze;
   
   private static final int START_X = 10; // where to start drawing maze in frame
   private static final int START_Y = 10;
   private static final int BOX_WIDTH = 20;  // width and height of one maze unit
   private static final int BOX_HEIGHT = 20;
   private static final int INSET = 2; 
   private static final int BORDER = 2;
                    // how much smaller on each side to make entry/exit inner box
   /**
      Constructs the component.
      @param maze   the maze to display
   */
   public MazeComponent(Maze maze) 
   {   
      this.maze = maze;
   }

   
   /**
     Draws the current state of maze including the path through it if one has
     been found.
     @param g the graphics context
   */
   public void paintComponent(Graphics g)
   {
       Graphics2D g2 = (Graphics2D) g;
       
       //Draws a black rectangle of the size of the maze plus a border. 
       Rectangle rect = new Rectangle(START_X-BORDER, START_Y-BORDER, BOX_WIDTH*maze.numCols()+BORDER*2, BOX_HEIGHT*maze.numRows()+BORDER*2);
       drawSingleRec(g2,Color.BLACK,rect);
       
       //Iterate through maze data and paints the white halls (coordinates without walls).
       g2.setColor(Color.WHITE);
       for(int i =0; i <this.maze.numRows(); i++)
       {
           for(int j =0; j<this.maze.numCols(); j++)
           {
               if(!this.maze.hasWallAt(new MazeCoord(i,j)))
               {
                   Rectangle body = new Rectangle(START_X+(BOX_WIDTH*(j)), START_Y+(BOX_HEIGHT*(i)),BOX_WIDTH, BOX_HEIGHT);
                   drawSingleRec(g2,Color.WHITE,body);
               }       
           }
       }
       
       //Paint the entry location. 
       Rectangle entry = new Rectangle(START_X+INSET+(BOX_WIDTH*(maze.getEntryLoc().getCol())), START_Y+INSET+(BOX_HEIGHT*(maze.getEntryLoc().getRow())),BOX_WIDTH-INSET*2, BOX_HEIGHT-INSET*2);
       drawSingleRec(g2,Color.YELLOW,entry);
       
       //Paint the exit location.
       Rectangle exit = new Rectangle(START_X+INSET+(BOX_WIDTH*(maze.getExitLoc().getCol())), START_Y+INSET+(BOX_HEIGHT*(maze.getExitLoc().getRow())),BOX_WIDTH-INSET*2, BOX_HEIGHT-INSET*2);
       drawSingleRec(g2,Color.GREEN,exit);
       
     //Called after a path has been found. Iterate through the path to draw a line between each two points.  
       if(maze.getPath().size() >0)                              
       {                           
          g2.setColor(Color.BLUE);
          ListIterator<MazeCoord> it = maze.getPath().listIterator(); 
          MazeCoord coord1 = it.next();
          while(it.hasNext())
          {
              MazeCoord coord2 = it.next();
              int x1 = START_X+(BOX_WIDTH*coord1.getCol())+BOX_WIDTH/2;
              int y1 = START_Y+(BOX_HEIGHT*coord1.getRow()+BOX_HEIGHT/2);
              int x2 = START_X+(BOX_WIDTH*coord2.getCol())+BOX_WIDTH/2;
              int y2 = START_Y+(BOX_HEIGHT*coord2.getRow()+BOX_HEIGHT/2);       
              
              g2.drawLine(x1, y1, x2, y2);
              coord1=coord2;   
          }
       }
   }
   
   /**
   Draws a rectangle and fill it of a given color. 
   @param g2 the graphics2D component.
   @param mycolor color to fill the rectangle.
   @param rect rectangle to be painted.
   */
   private void drawSingleRec(Graphics2D g2, Color mycolor, Rectangle rect)
   {
       g2.setColor(mycolor);
       g2.fill(rect);
       g2.draw(rect); 
   }
}



