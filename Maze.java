// Name:Alejandro Monroy Reyes
// USC loginid: monroyre
// CS 455 PA3
// Fall 2016

import java.util.LinkedList;
import java.util.ArrayList;


/**
   Maze class

   Stores information about a maze and can find a path through the maze
   (if there is one).
   
   Assumptions about structure of the maze, as given in mazeData, startLoc, and endLoc
   (parameters to constructor), and the path:
     -- no outer walls given in mazeData -- search assumes there is a virtual 
        border around the maze (i.e., the maze path can't go outside of the maze
        boundaries)
     -- start location for a path is maze coordinate startLoc
     -- exit location is maze coordinate exitLoc
     -- mazeData input is a 2D array of booleans, where true means there is a wall
        at that location, and false means there isn't (see public FREE / WALL 
        constants below) 
     -- in mazeData the first index indicates the row. e.g., mazeData[row][col]
     -- only travel in 4 compass directions (no diagonal paths)
     -- can't travel through walls
 */

public class Maze {
   
   public static final boolean FREE = false;
   public static final boolean WALL = true;
   
   private boolean mazeData[][];
   private MazeCoord startLoc;
   private MazeCoord endLoc;
   private LinkedList<MazeCoord> path;
   private boolean[][] visited;

   /**
   Constructs a maze.
   @param mazeData the maze to search.  See general Maze comments for what
   goes in this array.
   @param startLoc the location in maze to start the search (not necessarily on an edge)
   @param endLoc the "exit" location of the maze (not necessarily on an edge)
   PRE: 0 <= startLoc.getRow() < mazeData.length and 0 <= startLoc.getCol() < mazeData[0].length
      and 0 <= endLoc.getRow() < mazeData.length and 0 <= endLoc.getCol() < mazeData[0].length

   */
   public Maze(boolean[][] mazeData, MazeCoord startLoc, MazeCoord endLoc)
   {
       this.mazeData = mazeData;
       this.startLoc = startLoc;
       this.endLoc = endLoc; 
       this.path = new LinkedList<MazeCoord>();
       this.visited = new boolean[mazeData.length][mazeData[0].length];
   }


   /**
   Returns the number of rows in the maze
   @return number of rows
   */
   public int numRows() {
      return mazeData.length;
   }

   
   /**
   Returns the number of columns in the maze
   @return number of columns
   */   
   public int numCols() {
     return mazeData[0].length;   
   } 
 
   
   /**
   Returns true iff there is a wall at this location
   @param loc the location in maze coordinates
   @return whether there is a wall here
   PRE: 0 <= loc.getRow() < numRows() and 0 <= loc.getCol() < numCols()
   */
   public boolean hasWallAt(MazeCoord loc) {
       return (mazeData[loc.getRow()][loc.getCol()] == WALL);   
   }
   

   /**
   Returns the entry location of this maze.
   */
   public MazeCoord getEntryLoc() {
      return this.startLoc;   
   }
   
   
   /**
   Returns the exit location of this maze.
   */
   public MazeCoord getExitLoc() {
      return this.endLoc;   
   }

   
   /**
      Returns the path through the maze. First element is starting location, and
      last element is exit location.  If there was not path, or if this is called
      before search, returns empty list.
    
      @return the maze path
    */
   public LinkedList<MazeCoord> getPath() {
       return path;
   }


   /**
   Initializes the variables path and visited with the entry location and 
   calls the function findPath.
   @return whether path was found.
   */
   public boolean search()  {  
       if(path.isEmpty())
       {
       path.add(this.getEntryLoc());
       this.visited[this.getEntryLoc().getRow()][this.getEntryLoc().getCol()] = WALL;
       }
       return findPath(path.getLast());
      
   }
   
   /**
   Find a path through the maze if there is one.  Client can access the
   path found via getPath method.
   @param coord the location in maze coordinates
   @return whether path was found.
   */
   private boolean findPath(MazeCoord coord)
   {
       if(hasWallAt(coord)) 
       {
           return true;
       }
       if(coord.equals(this.getExitLoc()))
       { 
           path.add(coord);
           return true;
       }
       this.visited[coord.getRow()][coord.getCol()] = true; 
       path.add(coord);
       ArrayList<MazeCoord> neighborsFound = findNeighbors(coord);
       for(int i =0 ; i <neighborsFound.size(); i++)
       {
              if(inBounds(neighborsFound.get(i)) && !hasBeenVisited(neighborsFound.get(i)) && !hasWallAt(neighborsFound.get(i)))
              {
                      if(findPath(neighborsFound.get(i))== true)
                      {
                          return true;
                      }
                      else
                      {
                          path.removeLast();
                      }
              }
       }
       return false;
   }
   
   /**
   Returns an ArrayList containing the neighbors found for a given (r,c) MazeCoord coord.
   @param coord the location in maze coordinates
   @return Neighbors found.
   */
   private ArrayList<MazeCoord> findNeighbors(MazeCoord Coord)
   {
       ArrayList<MazeCoord> neighborsFound = new ArrayList<MazeCoord>();
       neighborsFound.add(new MazeCoord(Coord.getRow()-1, Coord.getCol()));     
       neighborsFound.add(new MazeCoord(Coord.getRow()+1, Coord.getCol()));
       neighborsFound.add(new MazeCoord(Coord.getRow(), Coord.getCol()-1));
       neighborsFound.add(new MazeCoord(Coord.getRow(), Coord.getCol()+1));
       return neighborsFound;
   }
   
   /**
   Determine if a given coord is in the bounds of the labyrinth. 
   @param coord the location in maze coordinates
   @return whether coord is in bounds.
   */
   private boolean inBounds(MazeCoord Coord)
   {
       if(Coord.getCol() < numCols() && Coord.getCol()>=0 && Coord.getRow() < numRows() && Coord.getRow()>=0)  
       {
           return true;
       }
       return false;
   }
   
   /**
   Determine if a given coord has been visited before. 
   @param coord the location in maze coordinates
   @return whether coord has been visited.
   */
   private boolean hasBeenVisited(MazeCoord Coord)
   {
       if(this.visited[Coord.getRow()][Coord.getCol()] == true)
       {
           return true;
       }
       return false;
   }

}